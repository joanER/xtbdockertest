# xtb binaries contained on Linux distro (UBUNTU 16.04) with docker

I created an Ubuntu 16.04 modified image where the xtb binaries (avaliable [here](https://github.com/grimme-lab) with docs [here](https://xtb-docs.readthedocs.io/en/latest/contents.html#)).
Built and tested on macOS Mojave (10.14.6).

You will need to: 

1. Install and open Docker on your system, for mac open this [link](https://docs.docker.com/docker-for-mac/install/)
2. Download the image available in this repo, available in the ```Ubuntu``` directory.
   - ```cd Ubuntu```
3. Load it with docker: ```docker load < imagename.tar``` 
4. Once the image is loaded you should already be able to run it! There are some xyz files:
   - ```cd test```
6. For each calculation run a command like the one below: 


```docker container run -t --rm -v $PWD:/tmp -c 4 xtb_623_ubuntu:1.1  <COMMANDS> <EXTRA>```

where 

1. ```-t --rm -v $PWD:/tmp -c 4``` are docker commands meaning: terminal, rm container after run (to save space), -v LOCALdir:CONTAINERdir is the communication channel between the local dir ($PWD, choose it at your will!) and */tmp* is the WORKDIR in the container (where stuff is run).
2. COMMANDS are the xtb related ones, e.g. ```xtb coord.xyz --``` would be a sp calculation
3. EXTRA are termina realted:
    - ```blank```: you will see the termianl out in your terminal    
    - ```&> dockerrun.log```: will save the tereminal output that shoud appear in your terminal into a file.               
    - ```| tee dockerrun.log```: will save the terminal output and will show it in terminal too                                  

You can hide ```docker container run -t --rm -v $PWD:/tmp -c 4 xtb_623_ubuntu:1.1``` in an alias or bash script for the sake of clarity.

(see docker and xtb docs for more info)    